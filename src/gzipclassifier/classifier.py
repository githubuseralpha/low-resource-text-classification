import gzip
import lzma
import bz2

import numpy as np

class TextClassifier:
    def __init__(self, algorithm="gzip", k=5) -> None:
        assert algorithm in ("gzip", "bz2", "lzma"), f"No such compression algorithm as {algorithm} available"
        self._algorithm = algorithm
        self.k = k 
        
        self.c_train = []
        self.training_data = []
        
    def fit(self, X, y):
        for text in X:
            self.c_train.append(len(self.compress(text)))
        
        self.training_data = np.stack((X, y), axis=1)
    
    def predict(self, X):
        labels = []
        for test_text in X:
            c_test = len(self.compress(test_text))
            distances = []
            for i, (train_text, _) in enumerate(self.training_data):
                concat_text = test_text + train_text
                c_concat = len(self.compress(concat_text))
                distance = (c_concat - min(c_test, self.c_train[i])) / max(c_test, self.c_train[i])
                distances.append(distance)
            sorted_indices = np.argsort(distances)
            top_k = self.training_data[sorted_indices[:self.k], 1]
            label = max(set(top_k), key=list(top_k).count)
            labels.append(label)
        return np.array(labels)
    
    def compress(self, text):
        if self._algorithm == "gzip":
            return gzip.compress(text.encode())
        elif self._algorithm == "lzma":
            return lzma.compress(text.encode())
        elif self._algorithm == "bz2":
            return bz2.compress(text.encode())
