# Low resource text classification

Code here presents a simple and lightweight solution for text classification. It is my implementation of this paper: https://aclanthology.org/2023.findings-acl.426.pdf. 

Demo directory contains example usage of presented package - Method is tested against AG news dataset.

## Instalation

```
pip install .
```
