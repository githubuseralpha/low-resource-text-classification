from setuptools import setup

setup(
    name = 'gzipclassifier',
    version = "1.0.0",
    author = 'Patryk Swiat',
    author_email = 'patrykswiat@gmail.com',
    description = 'Python package for text classsification',
    long_description = 'Python package for text classsification',
    license = "MIT license",
    py_modules=["gzipclassifier"],
    package_dir={"": "src"},
    install_requires = [
        "numpy",
        "pandas"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]  # Update these accordingly
)
